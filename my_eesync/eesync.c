#include <loadcore.h>
#include <sifman.h>

#define MODNAME "SyncEE"
IRX_ID(MODNAME, 0x01, 0x01);

extern struct irx_export_table _exp_eesync;

int PostResetCallback()
{
  sceSifSetSMFlag(0x40000);
  return 0;
}
int _start(int argc, char **argv)
{
  loadcore20(PostResetCallback, 2, 0);
  return MODULE_RESIDENT_END;
}
