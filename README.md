# ESR
![esr logo](img/esr-logo.png)

## Quick introduction
This repository contains the sources of ESR, starting at r10e. For now only IOP side, but I'll add the EE side as well once I'll make it readable.

## Contents

### `my_eesync`
Very simple EESYNC module replacement with disabled transfers into memory occupied by ESR.

### `my_imgdrv`
Very simple `rom` driver, that supports only operations that are needed to successfully read IOPRP image during IOP reset.

### `cdvdv_bios`
Simple module, that is intended for cdvdman in rom0. It has one serious flaw - makes the reads synchronous (it's not necessary, rather an effect of its simple design... but this improves stability a lot, which is essential, since this module is used during IOP resets). This one has not been changed pretty much since the very first version. Most homebrew apps use EELOADCNF to load xcdvdman which works fine with the other module and I know of no games that would require this one to be used.

### `pcdvdv_sce`
Patcher module intended for **xcdvdman** and **cdvdman** versions used by games. It takes care of all the initial patching, then quits (**without becoming resident**). It was created, so that the main resident module can be as small, as possible. Remember, that in case of some games, there is **less that 200-300 bytes of memory left** after allocating single sector buffer. It's not pretty, but it gets the job done. **Size of this file doesn't matter**, as it doesn't stay resident, so adding new patches, etc, is an option.

### `cdvdv_sce`
**Main ESR resident module**. It's been written from scratch for version 10. Whatever you do to it, try to **keep it as small as possible and make it use as little memory as possible**. It has some oddities or potentially unsafe code here and there to make its size smaller, but none of these should cause issues in actual games (considering the number of games tested, I'd say it's very unlikely). With current methods (patching and hooking rather than replacing modules), if all safeguards were added, at least 10-15 games I know would stop working, because there would be not enough memory to allocate even a single sector buffer. It's a trade-off.

> Do not mind the *sce* in names. It simply suggests that it's meant for new modules.

