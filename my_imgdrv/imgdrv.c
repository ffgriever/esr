#include <types.h>
#include <sysclib.h>
#include <loadcore.h>
#include <ioman.h>

unsigned int ioprpimg = 0xDEC1DEC1;
int ioprpsiz = 0xDEC2DEC2;

int dummy_fs()
{
  return 0;
}

int lseek_fs(iop_file_t *fd, unsigned long offset, int whence)
{
  if (whence == SEEK_END)
  {
    return ioprpsiz;
  }
  else
  {
    return 0;
  }
}
int read_fs(iop_file_t *fd, void *buffer, int size)
{
  memcpy(buffer, (void *)ioprpimg, size);
  return size;
}
typedef struct _iop_device_ffg
{
  const char *name;
  unsigned int type;
  unsigned int version;
  const char *desc;
  struct _iop_device_ops_ffg *ops;
} iop_device_ffg_t;

typedef struct _iop_device_ops_ffg
{
  int (*init)(iop_device_t *);
  int (*deinit)(iop_device_t *);
  int (*format)(iop_file_t *);
  int (*open)(iop_file_t *, const char *, int);
  int (*close)(iop_file_t *);
  int (*read)(iop_file_t *, void *, int);
  int (*write)(iop_file_t *, void *, int);
  int (*lseek)(iop_file_t *, unsigned long, int);
} iop_device_ops_ffg_t;

iop_device_ops_t my_device_ops = {
    dummy_fs, //init
    dummy_fs, //deinit
    NULL,     //format
    dummy_fs, //open
    dummy_fs, //close
    read_fs,  //read
    NULL,     //write
    lseek_fs, //lseek
};

const u8 name[] = "img";
iop_device_t my_device = {
    name,
    IOP_DT_FS,
    1,
    name,
    &my_device_ops};

int _start(int argc, char **argv)
{
  AddDrv((iop_device_t *)&my_device);
  return MODULE_RESIDENT_END;
}
