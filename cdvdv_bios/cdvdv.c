#include <stdio.h>
#include <sysclib.h>
#include <intrman.h>
#include <cdvdman.h>
#include <loadcore.h>
#include "loadcore_r.h"

#define MODNAME "dvdvmod"
IRX_ID(MODNAME, 0x01, 0x00);

u8 cdvdRead(u8 num)
{
	return (*(vu8 *)(0xbf402000 + num));
}

int hookDisktype()
{
	int ret = cdvdRead(0x0f);
	if (ret == 0xFE)
		ret = 0x14;
	return ret;
}

int (*pDefCdRead)(u32 lsn, u32 sectors, void *buf, cd_read_mode_t *mode);

int readSync(u32 lsn, u32 sectors, void *buf, cd_read_mode_t *mode)
{
	if (sceCdReadDVDV(lsn, sectors, buf, mode))
	{
		sceCdSync(0);
		return sceCdGetError();
	}
	else
	{
		return 1;
	}
}

//if there is an error, let the calling code handle retries
u8 sectorData[2064];
int hookCdRead(u32 lsn, u32 sectors, void *buf, cd_read_mode_t *mode)
{
	if (cdvdRead(0x0f) == 0xFE)
	{
		int i;
		u32 consecutiveSectors = (sectors > 2) ? (sectors * 2048) / 2064 : 0;
		if (consecutiveSectors > 0)
		{
			if (readSync(lsn, consecutiveSectors, buf, mode))
				return 0;
		}
		for (i = 0; i < consecutiveSectors; i++)
			memmove(buf + (2048 * i), buf + (2064 * i) + 12, 2048);

		for (i = consecutiveSectors; i < sectors; i++)
		{
			if (readSync(lsn + i, 1, sectorData, mode))
				return 0;
			memmove(buf + (2048 * i), sectorData + 12, 2048);
		}
		return 1;
	}
	else
	{
		return pDefCdRead(lsn, sectors, buf, mode);
	}
}

void initCdvd()
{
	u32 memAddr;
	void *exp;
	void *entryGetType;
	void *entryCdReadDVDV;
	int oldstate;
	u8 cdType = 0;

	exp = GetExportTable("cdvdman", 0x101);

	if (exp == NULL)
		exp = GetExportTable("cdvdman", 0x201);

	if (exp != NULL)
	{
		libhead *head = NULL;
		entryGetType = GetExportEntry(exp, 12);

		if (entryGetType != NULL)
		{
			CpuSuspendIntr(&oldstate);
			*(u32 *)entryGetType = 0x08000000 | (((u32)hookDisktype >> 2) & 0x03FFFFFF);
			*(u32 *)((u32)entryGetType + 4) = 0;
			CpuResumeIntr(oldstate);
		}
		pDefCdRead = GetExportEntry(exp, 6);
		HookExportEntry(exp, 6, hookCdRead);
		entryCdReadDVDV = GetExportEntry(exp, 20);

		head = (libhead *)(exp - 0x14);
		CpuSuspendIntr(&oldstate);
		ReleaseLibraryEntries_rom(head);
		RegisterLibraryEntries((void *)head);
		CpuResumeIntr(oldstate);
	}
	CpuSuspendIntr(&oldstate);
	for (memAddr = 0x0; memAddr < 0x1ffffc; memAddr += 4)
	{
		if (*(u32 *)memAddr == (0x0C000000 | (((u32)pDefCdRead >> 2) & 0x03FFFFFF)))
			*(u32 *)memAddr = (0x0C000000 | (((u32)hookCdRead >> 2) & 0x03FFFFFF));
	}
	CpuResumeIntr(oldstate);
	FlushDcache();
	FlushIcache();

	sceCdInit(1);

	cdType = cdvdRead(0x0f);

	while (cdType == 0xFF || cdType <= 0x05)
		cdType = cdvdRead(0x0f);
}

int _start(int argc, char **argv)
{
	initCdvd();
	return MODULE_RESIDENT_END;
}
