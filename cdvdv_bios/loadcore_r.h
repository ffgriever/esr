#ifndef __LOADCORE_R_H
#define __LOADCORE_R_H

#include <types.h>

typedef struct _libhead
{
  struct _libhead *next;
  struct _libcaller *client;
  unsigned short version;
  unsigned short flags;
  char name[8];
} libhead, LibInfo;

#define LIBMAGIC (0x41e00000)
typedef struct _libcaller
{
  unsigned long magic;
  struct _libcaller *client;
  unsigned short version;
  unsigned short flags;
  char name[8];
} libcaller;
#define MSTF_LOADED (0x0001)
#define MSTF_EXEC (0x0002)
#define MSTF_RESIDENT (0x0003)
#define MSTF_STOPPING (0x0004)
#define MSTF_SelfSTOPPING (0x0005)
#define MSTF_STOPPED (0x0006)
#define MSTF_SelfSTOPPED (0x0007)
#define MSTF_MASK(x) ((x)&0xf)

#define MSTF_REMOVABLE (0x0010)
#define MSTF_NOSYSALLOC (0x0020)
#define MSTF_CLEARMOD (0x0040)

void set_import_jumps(libcaller *caller);
int sub_fc4(libcaller *caller);
int checkMajorVerSame(libhead *libentry, libcaller *caller);
int checkSameNext(libhead *libentry, libcaller *caller);
int sub_1064(libcaller *caller, libhead *libentry);

void *GetExportTable(char *libname, int version);
u32 GetExportTableSize(void *table);
void *GetExportEntry(void *table, u32 entry);
void *HookExportEntry(void *table, u32 entry, void *func);
int ReleaseLibraryEntries_rom(libhead *exports);

#endif
