#include <loadcore.h>
#include <kerr.h>
#include <sysclib.h>
#include <intrman.h>
#include "loadcore_r.h"

/* Returns a pointer to a library entry table */
void *GetExportTable(char *libname, int version)
{
	if (libname != NULL)
	{
		iop_library_t lib;
		int i;
		char *psrc;

		memset(&lib, 0, sizeof(iop_library_t));
		lib.version = version;

		for (i = 0, psrc = libname; (i < 8) && (*psrc); i++, psrc++)
			lib.name[i] = *psrc;

		return QueryLibraryEntryTable(&lib);
	}

	return NULL;
}

/* Returns number of entries in the export table */
u32 GetExportTableSize(void *table)
{
	void **exp;
	u32 size;

	exp = (void **)table;
	size = 0;

	if (exp != NULL)
		while (*exp++ != NULL)
			size++;

	return size;
}

/* Returns an entry from the export table */
void *GetExportEntry(void *table, u32 entry)
{
	if (entry < GetExportTableSize(table))
	{
		void **exp;

		exp = (void **)table;

		return exp[entry];
	}

	return NULL;
}

/* Replaces an entry in the export table */
void *HookExportEntry(void *table, u32 entry, void *func)
{
	if (entry < GetExportTableSize(table))
	{
		int oldstate;
		void **exp, *temp;

		exp = (void **)table;
		exp = &exp[entry];

		CpuSuspendIntr(&oldstate);
		temp = *exp;
		*exp = func;
		func = temp;
		CpuResumeIntr(oldstate);

		return func;
	}

	return NULL;
}

//reversed loadcore
int ReleaseLibraryEntries_rom(libhead *exports) //loadcore 1.01 export 7
{
	libhead *LibraryEntryTable; //v1
	libcaller *templib = NULL;	//s2

	LibraryEntryTable = (libhead *)GetLibraryEntryTable();

	if (LibraryEntryTable->next == NULL)
	{ //loc_b00
		return KE_ERROR;
	}
	else
	{ //loc_ae4
		do
		{
			if (exports == LibraryEntryTable->next)
			{										 //loc_b08
				libcaller *client = exports->client; //s0

				exports->client = NULL;
				LibraryEntryTable->next = exports->next;
				exports->next = (libhead *)0x41c00000;
				if (client == 0)
				{
					return KE_OK;
				}
				else
				{
					(libhead *)(GetLibraryEntryTable() + 0x0C); //s2 <- Libent+2

					do
					{
						if (sub_fc4(client) != 0)
						{ //loc_b40
							templib = client->client;
							set_import_jumps(client);
							client->flags &= ~MSTF_EXEC;
							client->flags |= MSTF_STOPPING;
							client->client = (libcaller *)(((libhead *)((char *)GetLibraryEntryTable() + 0x0C))->next);
							((libhead *)((char *)GetLibraryEntryTable() + 0x0C))->next = (libhead *)client;
						}
					} while ((client = templib) != NULL);
					return KE_OK;
				}
			}
		} while ((LibraryEntryTable = LibraryEntryTable->next));
		return KE_ERROR;
	}
}
void set_import_jumps(libcaller *caller)
{
	caller = (libcaller *)((char *)caller + 0x14); //skip header and go straight to exports
	if (caller->magic == 0)
	{ //locret_f24
		return;
	}
	else
	{ //loc_eec
		do
		{
			if ((((unsigned int)(caller->client)) >> 26) != 9) //24000000 addiu $0, i
			{												   //locret_f24
				return;
			}

			//loc_f0c
			caller->magic = 0x3E00008; //jr $ra
			caller = (libcaller *)((char *)caller + 8);

		} while (caller->magic != 0);
	}
	return;
}

int sub_fc4(libcaller *caller)
{
	libhead *LibraryEntryTable = (libhead *)GetLibraryEntryTable(); //s0

	if (LibraryEntryTable == NULL)
	{ //loc_104c
		return KE_ERROR;
	}
	else
	{ //loc_fe4
		do
		{
			if (LibraryEntryTable->flags & MSTF_LOADED) //v0
			{
				if (checkSameNext(LibraryEntryTable, caller) != 0)
				{
					if (checkMajorVerSame(LibraryEntryTable, caller) == 0)
					{
						sub_1064(caller, LibraryEntryTable);
						caller->client = LibraryEntryTable->client;
						FlushIcache();
						LibraryEntryTable->client = caller;
						return KE_OK;
					}
				}
			}

			//loc_103c
			LibraryEntryTable = LibraryEntryTable->next;
		} while (LibraryEntryTable != 0);
	}
	return KE_ERROR;
}

int checkMajorVerSame(libhead *libentry, libcaller *caller)
{
	return ((libentry->version >> 8) - (caller->version >> 8));
}

int checkSameNext(libhead *libentry, libcaller *caller)
{
	if (caller->magic != (u32)libentry->next)
	{			  //locret_ebc
		return 0; //KE_OK
	}
	else
	{ //loc_ea8
		if (((*(unsigned int *)((char *)caller + 0x10)) ^ (*(unsigned int *)((char *)libentry + 0x10))) < 1)
			return 1;
		else
			return 0;
	}
}

int sub_1064(libcaller *caller, libhead *libentry)
{
	unsigned int i = 0;													//a3
	unsigned int *tempcaller = (unsigned int *)((char *)caller + 0x14); //a2

	libentry = (libhead *)((char *)libentry + 0x14);
	if (libentry->next != 0)
	{													  //loc_107c
		unsigned int *templib = (unsigned int *)libentry; //v1;

		do
		{
			templib++; //= 4;
			i++;
		} while (*templib != 0);
	}
	//loc_1094
	if ((*tempcaller) != 0)
	{ //loc_10a0
		do
		{
			if ((((*(unsigned int *)((char *)tempcaller + 0x04)) >> 26) & 0xFFFF) < i)
			{
				*tempcaller = ((((*(unsigned int *)(((((*(unsigned int *)((char *)tempcaller + 0x04)) >> 26) & 0xFFFF) << 2) + libentry)) >> 2) & 0x3FFFFFF) | 0x800);
			}
			else
			{ //loc_10f8
				*tempcaller = 0x3E00008;
			}
			//loc_10FC
			tempcaller = (unsigned int *)((char *)tempcaller + 0x08);
		} while (*tempcaller != 0);
	}

	//loc_1110
	caller->flags &= ~MSTF_STOPPING;
	caller->flags |= MSTF_EXEC;
	return KE_OK;
}
