#include <types.h>
#include <stdio.h>
#include <intrman.h>
#include <loadcore.h>
#include <cdvdman.h>
#include "esrimp.h"
#include "loadcore_r.h"
#include "../cdvdv_sce/scecdvdv.h"

#define DBGPRINTF(...)
//#define DBGPRINTF printf

#define MODNAME "ESR_DVDV_PA"
IRX_ID(MODNAME, 0x01, 0x01);

//u32 esrSpoofCD = 0xDEC03713;

u8 cdvdRead(u8 num)
{
  return (*(vu8 *)(0xbf402000 + num));
}

void **def_sceCdCallback,
    *hook_sceCdCallback,
    **def_sceCdstm0Cb,
    *hook_sceCdstm0Cb,
    **def_sceCdstm1Cb,
    *hook_sceCdstm1Cb,
    **def_sceCdRead0,
    *hook_sceCdRead0,
    **def_sceCdMmode,
    *hook_sceCdMmode,
    *discType,
    //*func_sceCdGetDiscType,
    **def_RegisterIntrHandler,
    *hook_RegisterIntrHandler,
    **def_AllocSysMemory,
    *hook_AllocSysMemory;

void *hookEntry(void *exp, u32 entry, void *hook)
{
  void *ret = GetExportEntry(exp, entry);
  HookExportEntry(exp, entry, hook);
  return ret;
}
void reRegisterEntries(void *exp)
{
  libhead *head;
  int oldstate, ret;

  head = (libhead *)(exp - 0x14);
  CpuSuspendIntr(&oldstate);
  ret = ReleaseLibraryEntries_rom(head);
  DBGPRINTF("Release ret = %d\n", ret);
  ret = RegisterLibraryEntries((void *)head);
  DBGPRINTF("Register ret = %d\n", ret);
  CpuResumeIntr(oldstate);
}

void initCdvd()
{
  u32 *memAddr;
  u8 currentDiscType;
  void *exp;
  int oldstate;

  //get default and hook addresses we'll need to patch later
  def_sceCdCallback = in_out_func(enum_def_sceCdCallback);
  hook_sceCdCallback = in_out_func(enum_hook_sceCdCallback);
  def_sceCdstm0Cb = in_out_func(enum_def_sceCdstm0Cb);
  hook_sceCdstm0Cb = in_out_func(enum_hook_sceCdstm0Cb);
  def_sceCdstm1Cb = in_out_func(enum_def_sceCdstm1Cb);
  hook_sceCdstm1Cb = in_out_func(enum_hook_sceCdstm1Cb);
  def_sceCdRead0 = in_out_func(enum_def_sceCdRead0);
  hook_sceCdRead0 = in_out_func(enum_hook_sceCdRead0);
  def_sceCdMmode = in_out_func(enum_def_sceCdMmode);
  hook_sceCdMmode = in_out_func(enum_hook_sceCdMmode);
  discType = in_out_func(enum_discType);
  def_RegisterIntrHandler = in_out_func(enum_def_RegisterIntrHandler);
  hook_RegisterIntrHandler = in_out_func(enum_hook_RegisterIntrHandler);
  def_AllocSysMemory = in_out_func(enum_def_AllocSysMemory);
  hook_AllocSysMemory = in_out_func(enum_hook_AllocSysMemory);

  exp = GetExportTable("cdvdman", 0x101);
  DBGPRINTF("exp = %08x\n", (unsigned int)exp);
  if (exp != NULL)
  {
    //func_sceCdGetDiscType = GetExportEntry(exp, 12);

    *def_sceCdCallback = hookEntry(exp, 37, hook_sceCdCallback);
    *def_sceCdstm0Cb = hookEntry(exp, 48, hook_sceCdstm0Cb);
    *def_sceCdstm1Cb = hookEntry(exp, 49, hook_sceCdstm1Cb);
    *def_sceCdRead0 = hookEntry(exp, 62, hook_sceCdRead0);
    *def_sceCdMmode = hookEntry(exp, 75, hook_sceCdMmode);
    reRegisterEntries(exp);
  }

  exp = GetExportTable("intrman", 0x102);
  DBGPRINTF("exp = 0x%08x\n", (unsigned int)exp);
  if (exp != NULL)
  {
    *def_RegisterIntrHandler = hookEntry(exp, 4, hook_RegisterIntrHandler);
    reRegisterEntries(exp);
  }

  exp = GetExportTable("sysmem", 0x101);
  DBGPRINTF("exp = 0x%08x\n", (unsigned int)exp);
  if (exp != NULL)
  {
    *def_AllocSysMemory = hookEntry(exp, 4, hook_AllocSysMemory);
    reRegisterEntries(exp);
  }

#if 0
  //set cdvdfsv into debug/verbose mode
  //the same can be done to force cdvdman to "talk" a little bit more
  exp = GetExportTable("cdvdfsv", 0x101);
  if (exp != NULL)
  {
    u32 addr_verbose;
    void *addr_cdvdfsv_4 = GetExportEntry(exp, 4);
    addr_verbose = *(u16*)addr_cdvdfsv_4;
    addr_verbose <<= 16;
    addr_verbose += *(s16*)(addr_cdvdfsv_4+4);
    *(u32*)addr_verbose = 1;
  }
#endif

  //Scanning entire IOP memory for direct uses of functions and values
  //(not using cdvdman's and cdvdfsv's exports). Make sure we're not
  //patching ourselves. This should be rewritten to something more extendable
  CpuSuspendIntr(&oldstate);
  for (memAddr = 0; memAddr < (u32 *)0x1ffffc; memAddr++)
  {
    if (*memAddr == (0x0C000000 | ((((u32)*def_sceCdRead0) >> 2) & 0x03FFFFFF))) //jmp sceCdRead0
    {
      DBGPRINTF("Found zero at: 0x%08x\n", (u32)memAddr);
      *memAddr = (0x0C000000 | (((u32)hook_sceCdRead0 >> 2) & 0x03FFFFFF));
    }
    else if (*memAddr == (0x0C000000 | ((((u32)*def_sceCdCallback) >> 2) & 0x03FFFFFF))) //jmp sceCdCallback
    {
      DBGPRINTF("Found call at: 0x%08x\n", (u32)memAddr);
      *memAddr = (0x0C000000 | (((u32)hook_sceCdCallback >> 2) & 0x03FFFFFF));
    }
    else if (*memAddr == (0x0C000000 | ((((u32)*def_sceCdstm1Cb) >> 2) & 0x03FFFFFF))) //jmp sceCdstm1cb
    {
      DBGPRINTF("Found 1cb at: 0x%08x\n", (u32)memAddr);
      *memAddr = (0x0C000000 | (((u32)hook_sceCdstm1Cb >> 2) & 0x03FFFFFF));
    }
    else if (*memAddr == (0x0C000000 | ((((u32)*def_sceCdstm0Cb) >> 2) & 0x03FFFFFF))) //jmp sceCdstm0cb
    {
      DBGPRINTF("Found 0cb at: 0x%08x\n", (u32)memAddr);
      *memAddr = (0x0C000000 | (((u32)hook_sceCdstm0Cb >> 2) & 0x03FFFFFF));
    }
    //all my reads use "lui v0/v1, 0xbf40" and "lbu v0/v1, 0x200f(v0/v1)"
    //instead of lui+ori+lbu combo, but always make sure
    else if (*memAddr == 0x3c02bf40 && *(memAddr + 1) == 0x3442200f) //lui v0, 0xBF40; ori v0, 0x200F;
    {
      DBGPRINTF("Found v0 at: 0x%08x\n", (u32)memAddr);
      *memAddr = 0x3c020000 | ((((u32)discType) >> 16) & 0xFFFF);
      *(memAddr + 1) = 0x34420000 | (((u32)discType) & 0xFFFF);
    }
    else if (*memAddr == 0x3c03bf40 && *(memAddr + 1) == 0x3463200f) //lui v1, 0xBF40; ori v1, 0x200F;
    {
      DBGPRINTF("Found v1 at: 0x%08x\n", (u32)memAddr);
      *memAddr = 0x3c030000 | ((((u32)discType) >> 16) & 0xFFFF);
      *(memAddr + 1) = 0x34630000 | (((u32)discType) & 0xFFFF);
    }
    //beqz reg, label
    //lui v0, 0xBF40
    //...code...
    //lui v0, 0xBF40?
    //label:
    //ori v0, 0x200F
    else if ((*memAddr & 0x10000000) && *(memAddr + 1) == 0x3c02bf40 && (*(memAddr + *(s16*)memAddr + 1) == 0x3442200f))
    {//it never happens with branch going back
      DBGPRINTF("Found beqz v0 at: 0x%08x\n", (u32)memAddr);
      *(memAddr + 1) = 0x3c020000 | (((u32)discType >> 16) & 0xFFFF);
      *(memAddr + *(s16*)memAddr + 1) = 0x34420000 | (((u32)discType) & 0xFFFF);
      if (*(memAddr + *(s16*)memAddr) == 0x3c02bf40)
        *(memAddr + *(s16*)memAddr) = *(memAddr + 1);
    }
#if 0
    if (esrSpoofCD != 0xDEC03713)
    {
      if (
          (*memAddr == (0x0C000000 | (((u32)func_sceCdGetDiscType >> 2) & 0x03FFFFFF))) &&
          ((*(memAddr - 2) & 0xffff0000) == 0xafb00000) &&
          ((*(memAddr - 1) & 0xffff0000) == 0xafbf0000) &&
          ((*(memAddr + 1)) == 0x00c08021))
      {
        DBGPRINTF("Found spoof at: 0x%08x\n", (u32)memAddr);
        *memAddr = 0x24020012;
      }
    }
#endif
  }
  CpuResumeIntr(oldstate);
  FlushDcache();
  FlushIcache();

  sceCdInit(1);

  do
  {
    currentDiscType = cdvdRead(0x0f);
  } while (currentDiscType == 0xFF || currentDiscType <= 0x05);
}

int _start(int argc, char **argv)
{
  initCdvd();
  return MODULE_NO_RESIDENT_END;
}
